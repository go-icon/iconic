package iconic

import (
	"fmt"
	"html/template"
)

// Icons is a list of all Iconic icons
var Icons = []string{"account-login", "account-logout", "action-redo", "action-undo", "align-center", "align-left", "align-right", "aperture", "arrow-bottom", "arrow-circle-bottom", "arrow-circle-left", "arrow-circle-right", "arrow-circle-top", "arrow-left", "arrow-right", "arrow-thick-bottom", "arrow-thick-left", "arrow-thick-right", "arrow-thick-top", "arrow-top", "audio", "audio-spectrum", "badge", "ban", "bar-chart", "basket", "battery-empty", "battery-full", "beaker", "bell", "bluetooth", "bold", "bolt", "book", "bookmark", "box", "briefcase", "british-pound", "browser", "brush", "bug", "bullhorn", "calculator", "calendar", "camera-slr", "caret-bottom", "caret-left", "caret-right", "caret-top", "cart", "chat", "check", "chevron-bottom", "chevron-left", "chevron-right", "chevron-top", "circle-check", "circle-x", "clipboard", "clock", "cloud", "cloud-download", "cloud-upload", "cloudy", "code", "cog", "collapse-down", "collapse-left", "collapse-right", "collapse-up", "command", "comment-square", "compass", "contrast", "copywriting", "credit-card", "crop", "dashboard", "data-transfer-download", "data-transfer-upload", "delete", "dial", "document", "dollar", "double-quote-sans-left", "double-quote-sans-right", "double-quote-serif-left", "double-quote-serif-right", "droplet", "eject", "elevator", "ellipses", "envelope-closed", "envelope-open", "euro", "excerpt", "expand-down", "expand-left", "expand-right", "expand-up", "external-link", "eye", "eyedropper", "file", "fire", "flag", "flash", "folder", "fork", "fullscreen-enter", "fullscreen-exit", "globe", "graph", "grid-four-up", "grid-three-up", "grid-two-up", "hard-drive", "header", "headphones", "heart", "home", "image", "inbox", "infinity", "info", "italic", "justify-center", "justify-left", "justify-right", "key", "laptop", "layers", "lightbulb", "link-broken", "link-intact", "list", "list-rich", "location", "lock-locked", "lock-unlocked", "loop", "loop-circular", "loop-square", "magnifying-glass", "map", "map-marker", "media-pause", "media-play", "media-record", "media-skip-backward", "media-skip-forward", "media-step-backward", "media-step-forward", "media-stop", "medical-cross", "menu", "microphone", "minus", "monitor", "moon", "move", "musical-note", "paperclip", "pencil", "people", "person", "phone", "pie-chart", "pin", "play-circle", "plus", "power-standby", "print", "project", "pulse", "puzzle-piece", "question-mark", "rain", "random", "reload", "resize-both", "resize-height", "resize-width", "rss", "rss-alt", "script", "share", "share-boxed", "shield", "signal", "signpost", "sort-ascending", "sort-descending", "spreadsheet", "star", "sun", "tablet", "tag", "tags", "target", "task", "terminal", "text", "thumb-down", "thumb-up", "timer", "transfer", "trash", "underline", "vertical-align-bottom", "vertical-align-center", "vertical-align-top", "video", "volume-high", "volume-low", "volume-off", "warning", "wifi", "wrench", "x", "yen", "zoom-in", "zoom-out"}

// Iconic represents an SVG node
type Iconic struct {
	xml    string
	width  int
	height int
	style  string
	id     string
	class  string
}

// XML returns the SVG node as an XML string
func (i *Iconic) XML() string {
	return fmt.Sprintf(i.xml, i.width, i.height, i.style, i.id, i.class)
}

// HTML returns the SVG node as an HTML template, safe for use in Go templates
func (i *Iconic) HTML() template.HTML {
	return template.HTML(i.XML())
}

// Size sets the size of an Iconic
// Short for calling Width and Height with the same int
func (i *Iconic) Size(size int) {
	i.Width(size)
	i.Height(size)
}

// Width sets the width of an Iconic
func (i *Iconic) Width(width int) {
	i.width = width
}

// Height sets the height of an Iconic
func (i *Iconic) Height(height int) {
	i.height = height
}

// Style sets the style of an Iconic
func (i *Iconic) Style(style string) {
	i.style = style
}

// Id sets the id of an Iconic
func (i *Iconic) Id(id string) {
	i.id = id
}

// Class sets the class of an Iconic
func (i *Iconic) Class(class string) {
	i.class = class
}

// Icon returns the named Iconic SVG node.
// It returns nil if name is not a valid Iconic symbol name.
func Icon(name string) *Iconic {
	switch name {
	case "account-login":
		return AccountLogin()
	case "account-logout":
		return AccountLogout()
	case "action-redo":
		return ActionRedo()
	case "action-undo":
		return ActionUndo()
	case "align-center":
		return AlignCenter()
	case "align-left":
		return AlignLeft()
	case "align-right":
		return AlignRight()
	case "aperture":
		return Aperture()
	case "arrow-bottom":
		return ArrowBottom()
	case "arrow-circle-bottom":
		return ArrowCircleBottom()
	case "arrow-circle-left":
		return ArrowCircleLeft()
	case "arrow-circle-right":
		return ArrowCircleRight()
	case "arrow-circle-top":
		return ArrowCircleTop()
	case "arrow-left":
		return ArrowLeft()
	case "arrow-right":
		return ArrowRight()
	case "arrow-thick-bottom":
		return ArrowThickBottom()
	case "arrow-thick-left":
		return ArrowThickLeft()
	case "arrow-thick-right":
		return ArrowThickRight()
	case "arrow-thick-top":
		return ArrowThickTop()
	case "arrow-top":
		return ArrowTop()
	case "audio":
		return Audio()
	case "audio-spectrum":
		return AudioSpectrum()
	case "badge":
		return Badge()
	case "ban":
		return Ban()
	case "bar-chart":
		return BarChart()
	case "basket":
		return Basket()
	case "battery-empty":
		return BatteryEmpty()
	case "battery-full":
		return BatteryFull()
	case "beaker":
		return Beaker()
	case "bell":
		return Bell()
	case "bluetooth":
		return Bluetooth()
	case "bold":
		return Bold()
	case "bolt":
		return Bolt()
	case "book":
		return Book()
	case "bookmark":
		return Bookmark()
	case "box":
		return Box()
	case "briefcase":
		return Briefcase()
	case "british-pound":
		return BritishPound()
	case "browser":
		return Browser()
	case "brush":
		return Brush()
	case "bug":
		return Bug()
	case "bullhorn":
		return Bullhorn()
	case "calculator":
		return Calculator()
	case "calendar":
		return Calendar()
	case "camera-slr":
		return CameraSlr()
	case "caret-bottom":
		return CaretBottom()
	case "caret-left":
		return CaretLeft()
	case "caret-right":
		return CaretRight()
	case "caret-top":
		return CaretTop()
	case "cart":
		return Cart()
	case "chat":
		return Chat()
	case "check":
		return Check()
	case "chevron-bottom":
		return ChevronBottom()
	case "chevron-left":
		return ChevronLeft()
	case "chevron-right":
		return ChevronRight()
	case "chevron-top":
		return ChevronTop()
	case "circle-check":
		return CircleCheck()
	case "circle-x":
		return CircleX()
	case "clipboard":
		return Clipboard()
	case "clock":
		return Clock()
	case "cloud":
		return Cloud()
	case "cloud-download":
		return CloudDownload()
	case "cloud-upload":
		return CloudUpload()
	case "cloudy":
		return Cloudy()
	case "code":
		return Code()
	case "cog":
		return Cog()
	case "collapse-down":
		return CollapseDown()
	case "collapse-left":
		return CollapseLeft()
	case "collapse-right":
		return CollapseRight()
	case "collapse-up":
		return CollapseUp()
	case "command":
		return Command()
	case "comment-square":
		return CommentSquare()
	case "compass":
		return Compass()
	case "contrast":
		return Contrast()
	case "copywriting":
		return Copywriting()
	case "credit-card":
		return CreditCard()
	case "crop":
		return Crop()
	case "dashboard":
		return Dashboard()
	case "data-transfer-download":
		return DataTransferDownload()
	case "data-transfer-upload":
		return DataTransferUpload()
	case "delete":
		return Delete()
	case "dial":
		return Dial()
	case "document":
		return Document()
	case "dollar":
		return Dollar()
	case "double-quote-sans-left":
		return DoubleQuoteSansLeft()
	case "double-quote-sans-right":
		return DoubleQuoteSansRight()
	case "double-quote-serif-left":
		return DoubleQuoteSerifLeft()
	case "double-quote-serif-right":
		return DoubleQuoteSerifRight()
	case "droplet":
		return Droplet()
	case "eject":
		return Eject()
	case "elevator":
		return Elevator()
	case "ellipses":
		return Ellipses()
	case "envelope-closed":
		return EnvelopeClosed()
	case "envelope-open":
		return EnvelopeOpen()
	case "euro":
		return Euro()
	case "excerpt":
		return Excerpt()
	case "expand-down":
		return ExpandDown()
	case "expand-left":
		return ExpandLeft()
	case "expand-right":
		return ExpandRight()
	case "expand-up":
		return ExpandUp()
	case "external-link":
		return ExternalLink()
	case "eye":
		return Eye()
	case "eyedropper":
		return Eyedropper()
	case "file":
		return File()
	case "fire":
		return Fire()
	case "flag":
		return Flag()
	case "flash":
		return Flash()
	case "folder":
		return Folder()
	case "fork":
		return Fork()
	case "fullscreen-enter":
		return FullscreenEnter()
	case "fullscreen-exit":
		return FullscreenExit()
	case "globe":
		return Globe()
	case "graph":
		return Graph()
	case "grid-four-up":
		return GridFourUp()
	case "grid-three-up":
		return GridThreeUp()
	case "grid-two-up":
		return GridTwoUp()
	case "hard-drive":
		return HardDrive()
	case "header":
		return Header()
	case "headphones":
		return Headphones()
	case "heart":
		return Heart()
	case "home":
		return Home()
	case "image":
		return Image()
	case "inbox":
		return Inbox()
	case "infinity":
		return Infinity()
	case "info":
		return Info()
	case "italic":
		return Italic()
	case "justify-center":
		return JustifyCenter()
	case "justify-left":
		return JustifyLeft()
	case "justify-right":
		return JustifyRight()
	case "key":
		return Key()
	case "laptop":
		return Laptop()
	case "layers":
		return Layers()
	case "lightbulb":
		return Lightbulb()
	case "link-broken":
		return LinkBroken()
	case "link-intact":
		return LinkIntact()
	case "list":
		return List()
	case "list-rich":
		return ListRich()
	case "location":
		return Location()
	case "lock-locked":
		return LockLocked()
	case "lock-unlocked":
		return LockUnlocked()
	case "loop":
		return Loop()
	case "loop-circular":
		return LoopCircular()
	case "loop-square":
		return LoopSquare()
	case "magnifying-glass":
		return MagnifyingGlass()
	case "map":
		return Map()
	case "map-marker":
		return MapMarker()
	case "media-pause":
		return MediaPause()
	case "media-play":
		return MediaPlay()
	case "media-record":
		return MediaRecord()
	case "media-skip-backward":
		return MediaSkipBackward()
	case "media-skip-forward":
		return MediaSkipForward()
	case "media-step-backward":
		return MediaStepBackward()
	case "media-step-forward":
		return MediaStepForward()
	case "media-stop":
		return MediaStop()
	case "medical-cross":
		return MedicalCross()
	case "menu":
		return Menu()
	case "microphone":
		return Microphone()
	case "minus":
		return Minus()
	case "monitor":
		return Monitor()
	case "moon":
		return Moon()
	case "move":
		return Move()
	case "musical-note":
		return MusicalNote()
	case "paperclip":
		return Paperclip()
	case "pencil":
		return Pencil()
	case "people":
		return People()
	case "person":
		return Person()
	case "phone":
		return Phone()
	case "pie-chart":
		return PieChart()
	case "pin":
		return Pin()
	case "play-circle":
		return PlayCircle()
	case "plus":
		return Plus()
	case "power-standby":
		return PowerStandby()
	case "print":
		return Print()
	case "project":
		return Project()
	case "pulse":
		return Pulse()
	case "puzzle-piece":
		return PuzzlePiece()
	case "question-mark":
		return QuestionMark()
	case "rain":
		return Rain()
	case "random":
		return Random()
	case "reload":
		return Reload()
	case "resize-both":
		return ResizeBoth()
	case "resize-height":
		return ResizeHeight()
	case "resize-width":
		return ResizeWidth()
	case "rss":
		return Rss()
	case "rss-alt":
		return RssAlt()
	case "script":
		return Script()
	case "share":
		return Share()
	case "share-boxed":
		return ShareBoxed()
	case "shield":
		return Shield()
	case "signal":
		return Signal()
	case "signpost":
		return Signpost()
	case "sort-ascending":
		return SortAscending()
	case "sort-descending":
		return SortDescending()
	case "spreadsheet":
		return Spreadsheet()
	case "star":
		return Star()
	case "sun":
		return Sun()
	case "tablet":
		return Tablet()
	case "tag":
		return Tag()
	case "tags":
		return Tags()
	case "target":
		return Target()
	case "task":
		return Task()
	case "terminal":
		return Terminal()
	case "text":
		return Text()
	case "thumb-down":
		return ThumbDown()
	case "thumb-up":
		return ThumbUp()
	case "timer":
		return Timer()
	case "transfer":
		return Transfer()
	case "trash":
		return Trash()
	case "underline":
		return Underline()
	case "vertical-align-bottom":
		return VerticalAlignBottom()
	case "vertical-align-center":
		return VerticalAlignCenter()
	case "vertical-align-top":
		return VerticalAlignTop()
	case "video":
		return Video()
	case "volume-high":
		return VolumeHigh()
	case "volume-low":
		return VolumeLow()
	case "volume-off":
		return VolumeOff()
	case "warning":
		return Warning()
	case "wifi":
		return Wifi()
	case "wrench":
		return Wrench()
	case "x":
		return X()
	case "yen":
		return Yen()
	case "zoom-in":
		return ZoomIn()
	case "zoom-out":
		return ZoomOut()
	default:
		return nil
	}
}

// AccountLogin returns the "account-login" Iconic.
func AccountLogin() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>account-login</title>
  <path d="M3 0v1h4v5h-4v1h5v-7h-5zm1 2v1h-4v1h4v1l2-1.5-2-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// AccountLogout returns the "account-logout" Iconic.
func AccountLogout() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>account-logout</title>
  <path d="M3 0v1h4v5h-4v1h5v-7h-5zm-1 2l-2 1.5 2 1.5v-1h4v-1h-4v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ActionRedo returns the "action-redo" Iconic.
func ActionRedo() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>action-redo</title>
  <path d="M3.5 0c-1.93 0-3.5 1.57-3.5 3.5 0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v.5h-1l2 2 2-2h-1v-.5c0-1.93-1.57-3.5-3.5-3.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ActionUndo returns the "action-undo" Iconic.
func ActionUndo() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>action-undo</title>
  <path d="M4.5 0c-1.93 0-3.5 1.57-3.5 3.5v.5h-1l2 2 2-2h-1v-.5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5c0-1.93-1.57-3.5-3.5-3.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// AlignCenter returns the "align-center" Iconic.
func AlignCenter() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>align-center</title>
  <path d="M0 0v1h8v-1h-8zm1 2v1h6v-1h-6zm-1 2v1h8v-1h-8zm1 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// AlignLeft returns the "align-left" Iconic.
func AlignLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>align-left</title>
  <path d="M0 0v1h8v-1h-8zm0 2v1h6v-1h-6zm0 2v1h8v-1h-8zm0 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// AlignRight returns the "align-right" Iconic.
func AlignRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>align-right</title>
  <path d="M0 0v1h8v-1h-8zm2 2v1h6v-1h-6zm-2 2v1h8v-1h-8zm2 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Aperture returns the "aperture" Iconic.
func Aperture() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>aperture</title>
  <path d="M4 0c-.69 0-1.34.19-1.91.5l3.22 2.34.75-2.25c-.6-.36-1.31-.59-2.06-.59zm-2.75 1.13c-.76.73-1.25 1.74-1.25 2.88 0 .25.02.48.06.72l3.09-2.22-1.91-1.38zm5.63.13l-1.22 3.75h2.19c.08-.32.16-.65.16-1 0-1.07-.44-2.03-1.13-2.75zm-4.72 3.22l-1.75 1.25c.55 1.13 1.6 1.99 2.88 2.22l-1.13-3.47zm1.56 1.53l.63 1.97c1.33-.12 2.46-.88 3.09-1.97h-3.72z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowBottom returns the "arrow-bottom" Iconic.
func ArrowBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-bottom</title>
  <path d="M2 0v5h-2l2.53 3 2.47-3h-2v-5h-1z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowCircleBottom returns the "arrow-circle-bottom" Iconic.
func ArrowCircleBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-circle-bottom</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm-1 1h2v3h2l-3 3-3-3h2v-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowCircleLeft returns the "arrow-circle-left" Iconic.
func ArrowCircleLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-circle-left</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1v2h3v2h-3v2l-3-3 3-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowCircleRight returns the "arrow-circle-right" Iconic.
func ArrowCircleRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-circle-right</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1l3 3-3 3v-2h-3v-2h3v-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowCircleTop returns the "arrow-circle-top" Iconic.
func ArrowCircleTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-circle-top</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1l3 3h-2v3h-2v-3h-2l3-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowLeft returns the "arrow-left" Iconic.
func ArrowLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-left</title>
  <path d="M3 0l-3 2.53 3 2.47v-2h5v-1h-5v-2z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowRight returns the "arrow-right" Iconic.
func ArrowRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-right</title>
  <path d="M5 0v2h-5v1h5v2l3-2.53-3-2.47z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowThickBottom returns the "arrow-thick-bottom" Iconic.
func ArrowThickBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-thick-bottom</title>
  <path d="M2 0v5h-2l3.03 3 2.97-3h-2v-5h-2z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowThickLeft returns the "arrow-thick-left" Iconic.
func ArrowThickLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-thick-left</title>
  <path d="M3 0l-3 3.03 3 2.97v-2h5v-2h-5v-2z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowThickRight returns the "arrow-thick-right" Iconic.
func ArrowThickRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-thick-right</title>
  <path d="M5 0v2h-5v2h5v2l3-3.03-3-2.97z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowThickTop returns the "arrow-thick-top" Iconic.
func ArrowThickTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-thick-top</title>
  <path d="M2.97 0l-2.97 3h2v5h2v-5h2l-3.03-3z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ArrowTop returns the "arrow-top" Iconic.
func ArrowTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>arrow-top</title>
  <path d="M2.47 0l-2.47 3h2v5h1v-5h2l-2.53-3z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Audio returns the "audio" Iconic.
func Audio() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>audio</title>
  <path d="M1.16 0c-.72.72-1.16 1.71-1.16 2.81s.43 2.12 1.16 2.84l.72-.72c-.54-.54-.88-1.29-.88-2.13 0-.83.33-1.55.88-2.09l-.72-.72zm5.69 0l-.72.72c.54.54.88 1.26.88 2.09 0 .83-.33 1.58-.88 2.13l.72.72c.72-.72 1.16-1.74 1.16-2.84 0-1.1-.43-2.09-1.16-2.81zm-4.25 1.41c-.36.36-.59.86-.59 1.41 0 .55.23 1.08.59 1.44l.69-.72c-.18-.18-.28-.44-.28-.72 0-.28.1-.5.28-.69l-.69-.72zm2.81 0l-.69.72c.18.18.28.41.28.69 0 .28-.1.54-.28.72l.69.72c.36-.36.59-.89.59-1.44 0-.55-.23-1.05-.59-1.41z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// AudioSpectrum returns the "audio-spectrum" Iconic.
func AudioSpectrum() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>audio-spectrum</title>
  <path d="M4 0v8h1v-8h-1zm-2 1v6h1v-6h-1zm4 1v4h1v-4h-1zm-6 1v2h1v-2h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Badge returns the "badge" Iconic.
func Badge() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>badge</title>
  <path d="M2 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-1 4.81v3.19l1-1 1 1v-3.19c-.31.11-.65.19-1 .19s-.69-.08-1-.19z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Ban returns the "ban" Iconic.
func Ban() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>ban</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c.66 0 1.26.21 1.75.56l-4.19 4.19c-.35-.49-.56-1.09-.56-1.75 0-1.66 1.34-3 3-3zm2.44 1.25c.35.49.56 1.09.56 1.75 0 1.66-1.34 3-3 3-.66 0-1.26-.21-1.75-.56l4.19-4.19z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// BarChart returns the "bar-chart" Iconic.
func BarChart() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bar-chart</title>
  <path d="M0 0v7h8v-1h-7v-6h-1zm5 0v5h2v-5h-2zm-3 2v3h2v-3h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Basket returns the "basket" Iconic.
func Basket() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>basket</title>
  <path d="M3.97 0c-.13.01-.26.08-.34.19l-2.34 2.81h-1.28v1h1v3.66c0 .18.16.34.34.34h5.31c.18 0 .34-.16.34-.34v-3.66h1v-1h-1.28c-.27-.33-2.39-2.86-2.41-2.88-.11-.09-.22-.14-.34-.13zm.03 1.28l1.44 1.72h-2.88l1.44-1.72zm-1.5 3.72c.28 0 .5.22.5.5v1c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-1c0-.28.22-.5.5-.5zm3 0c.28 0 .5.22.5.5v1c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-1c0-.28.22-.5.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// BatteryEmpty returns the "battery-empty" Iconic.
func BatteryEmpty() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>battery-empty</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v5.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-1.91h1v-2h-1v-1.91c0-.06-.04-.09-.09-.09h-6.81zm.91 1h5v4h-5v-4z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// BatteryFull returns the "battery-full" Iconic.
func BatteryFull() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>battery-full</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v5.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-1.91h1v-2h-1v-1.91c0-.06-.04-.09-.09-.09h-6.81z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Beaker returns the "beaker" Iconic.
func Beaker() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>beaker</title>
  <path d="M1.34 0a.5.5 0 0 0 .16 1h.5v1.41c-.09.17-1.2 2.31-1.66 3.09-.16.26-.34.61-.34 1.06 0 .39.15.77.41 1.03s.64.41 1.03.41h5.13c.38 0 .74-.16 1-.41h.03c.26-.26.41-.64.41-1.03 0-.45-.19-.8-.34-1.06-.46-.78-1.57-2.92-1.66-3.09v-1.41h.5a.5.5 0 1 0 0-1h-5a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0zm1.66 1h2v1.63l.06.09s.63 1.23 1.19 2.28h-4.5c.56-1.05 1.19-2.28 1.19-2.28l.06-.09v-1.63z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bell returns the "bell" Iconic.
func Bell() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bell</title>
  <path d="M4 0c-1.1 0-2 .9-2 2 0 1.04-.52 1.98-1.34 2.66-.41.34-.66.82-.66 1.34h8c0-.52-.24-1-.66-1.34-.82-.68-1.34-1.62-1.34-2.66 0-1.1-.89-2-2-2zm-1 7c0 .55.45 1 1 1s1-.45 1-1h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bluetooth returns the "bluetooth" Iconic.
func Bluetooth() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bluetooth</title>
  <path d="M1.5 0v2.5l-.75-.75-.75.75 1.5 1.5-1.5 1.5.75.75.75-.75v2.5h.5l3.5-2.5-2.25-1.53 2.25-1.47-3.5-2.5h-.5zm1 1.5l1.5 1-1.5 1v-2zm0 3l1.5 1-1.5 1v-2z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bold returns the "bold" Iconic.
func Bold() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bold</title>
  <path d="M0 0v1c.55 0 1 .45 1 1v4c0 .55-.45 1-1 1v1h5.5c1.38 0 2.5-1.12 2.5-2.5 0-1-.59-1.85-1.44-2.25.27-.34.44-.78.44-1.25 0-1.1-.89-2-2-2h-5zm3 1h1c.55 0 1 .45 1 1s-.45 1-1 1h-1v-2zm0 3h1.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-1.5v-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bolt returns the "bolt" Iconic.
func Bolt() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bolt</title>
  <path d="M3 0l-3 5h2v3l3-5h-2v-3z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Book returns the "book" Iconic.
func Book() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>book</title>
  <path d="M1 0c-.07 0-.13.01-.19.03-.39.08-.7.39-.78.78-.03.06-.03.12-.03.19v5.5c0 .83.67 1.5 1.5 1.5h5.5v-1h-5.5c-.28 0-.5-.22-.5-.5s.22-.5.5-.5h5.5v-5.5c0-.28-.22-.5-.5-.5h-.5v3l-1-1-1 1v-3h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bookmark returns the "bookmark" Iconic.
func Bookmark() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bookmark</title>
  <path d="M0 0v8l2-2 2 2v-8h-4z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Box returns the "box" Iconic.
func Box() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>box</title>
  <path d="M0 0v1h8v-1h-8zm0 2v5.91c0 .05.04.09.09.09h7.81c.05 0 .09-.04.09-.09v-5.91h-2.97v1.03h-2.03v-1.03h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Briefcase returns the "briefcase" Iconic.
func Briefcase() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>briefcase</title>
  <path d="M3 0c-.55 0-1 .45-1 1v1h-1.91c-.06 0-.09.04-.09.09v2.41c0 .28.22.5.5.5h7c.28 0 .5-.22.5-.5v-2.41c0-.06-.04-.09-.09-.09h-1.91v-1c0-.55-.45-1-1-1h-2zm0 1h2v1h-2v-1zm-3 4.91v2c0 .05.04.09.09.09h7.81c.05 0 .09-.04.09-.09v-2c-.16.06-.32.09-.5.09h-7c-.18 0-.34-.04-.5-.09z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// BritishPound returns the "british-pound" Iconic.
func BritishPound() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>british-pound</title>
  <path d="M3 0c-.62 0-1.16.26-1.5.69-.34.43-.5.99-.5 1.56 0 .69.16 1.25.25 1.75h-1.25v1h1.22c-.11.45-.37.96-1.06 1.66l-.16.13v1.22h6v-1h-4.91c.64-.73.98-1.4 1.13-2h1.78v-1h-1.72c-.08-.68-.28-1.24-.28-1.75 0-.39.11-.73.28-.94.17-.21.37-.31.72-.31.39 0 .61.11.75.25s.25.36.25.75h1c0-.58-.17-1.1-.53-1.47-.37-.37-.89-.53-1.47-.53z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Browser returns the "browser" Iconic.
func Browser() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>browser</title>
  <path d="M.34 0a.5.5 0 0 0-.34.5v7a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.5-.5h-7a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0zm1.16 1c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5zm2 0h3c.28 0 .5.22.5.5s-.22.5-.5.5h-3c-.28 0-.5-.22-.5-.5s.22-.5.5-.5zm-2.5 2h6v4h-6v-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Brush returns the "brush" Iconic.
func Brush() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>brush</title>
  <path d="M7.44.03c-.03 0-.04.02-.06.03l-3.75 2.66c-.04.03-.1.11-.13.16l-.13.25c.72.23 1.27.78 1.5 1.5l.25-.13c.05-.03.12-.08.16-.13l2.66-3.75c.03-.05.04-.09 0-.13l-.44-.44c-.02-.02-.04-.03-.06-.03zm-4.78 3.97c-.74 0-1.31.61-1.31 1.34 0 .99-.55 1.85-1.34 2.31.39.22.86.34 1.34.34 1.47 0 2.66-1.18 2.66-2.66 0-.74-.61-1.34-1.34-1.34z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bug returns the "bug" Iconic.
func Bug() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bug</title>
  <path d="M3.5 0c-1.19 0-1.98 1.69-1.19 2.5-.09.07-.2.14-.28.22l-1.31-.66a.5.5 0 0 0-.34-.06.5.5 0 0 0-.09.94l1.16.56c-.09.16-.19.33-.25.5h-.69a.5.5 0 0 0-.09 0 .5.5 0 1 0 .09 1h.5c0 .23.02.45.06.66l-.78.41a.5.5 0 1 0 .44.88l.66-.34c.25.46.62.85 1.03 1.09.35-.19.59-.44.59-.72v-1.44a.5.5 0 0 0 0-.09v-.81a.5.5 0 0 0 0-.22c.05-.23.26-.41.5-.41.28 0 .5.22.5.5v.88a.5.5 0 0 0 0 .09v.06a.5.5 0 0 0 0 .09v1.34c0 .27.24.53.59.72.41-.25.79-.63 1.03-1.09l.66.34a.5.5 0 1 0 .44-.88l-.78-.41c.04-.21.06-.43.06-.66h.5a.5.5 0 1 0 0-1h-.69c-.06-.17-.16-.34-.25-.5l1.16-.56a.5.5 0 0 0-.31-.94.5.5 0 0 0-.13.06l-1.31.66c-.09-.08-.19-.15-.28-.22.78-.83 0-2.5-1.19-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Bullhorn returns the "bullhorn" Iconic.
func Bullhorn() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>bullhorn</title>
  <path d="M6 0v6c.03.01.07 0 .09 0h.81c.05 0 .09-.04.09-.09v-5.81c0-.06-.04-.09-.09-.09h-.91zm-1 .5l-2.91 1.47c-.05.02-.13.03-.19.03h-1.81c-.06 0-.09.04-.09.09v1.81c0 .06.04.09.09.09h.91l1.03 2.72c.11.25.44.36.69.25.25-.11.36-.44.25-.69l-.75-1.78c.03-.14.13-.22.28-.22v-.03l2.5 1.25v-5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Calculator returns the "calculator" Iconic.
func Calculator() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>calculator</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v7.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-7.81c0-.06-.04-.09-.09-.09h-6.81zm.91 1h5v2h-5v-2zm0 3h1v1h-1v-1zm2 0h1v1h-1v-1zm2 0h1v3h-1v-3zm-4 2h1v1h-1v-1zm2 0h1v1h-1v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Calendar returns the "calendar" Iconic.
func Calendar() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>calendar</title>
  <path d="M0 0v2h7v-2h-7zm0 3v4.91c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-4.91h-7zm1 1h1v1h-1v-1zm2 0h1v1h-1v-1zm2 0h1v1h-1v-1zm-4 2h1v1h-1v-1zm2 0h1v1h-1v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CameraSlr returns the "camera-slr" Iconic.
func CameraSlr() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>camera-slr</title>
  <path d="M4.09 0c-.05 0-.1.04-.13.09l-.94 1.81c-.02.05-.07.09-.13.09h-1.41c-.83 0-1.5.67-1.5 1.5v4.41c0 .05.04.09.09.09h7.81c.05 0 .09-.04.09-.09v-5.81c0-.06-.04-.09-.09-.09h-.81c-.05 0-.1-.04-.13-.09l-.94-1.81c-.03-.05-.07-.09-.13-.09h-1.81zm-2.59 3c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5zm3.5 0c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm0 1c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CaretBottom returns the "caret-bottom" Iconic.
func CaretBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>caret-bottom</title>
  <path d="M0 0l4 4 4-4h-8z" transform="translate(0 2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CaretLeft returns the "caret-left" Iconic.
func CaretLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>caret-left</title>
  <path d="M4 0l-4 4 4 4v-8z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CaretRight returns the "caret-right" Iconic.
func CaretRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>caret-right</title>
  <path d="M0 0v8l4-4-4-4z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CaretTop returns the "caret-top" Iconic.
func CaretTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>caret-top</title>
  <path d="M4 0l-4 4h8l-4-4z" transform="translate(0 2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Cart returns the "cart" Iconic.
func Cart() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cart</title>
  <path d="M.34 0a.5.5 0 0 0 .16 1h1.5l.09.25.41 1.25.41 1.25c.04.13.21.25.34.25h3.5c.14 0 .3-.12.34-.25l.81-2.5c.04-.13-.02-.25-.16-.25h-4.44l-.38-.72a.5.5 0 0 0-.44-.28h-2a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0zm3.16 5c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm3 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Chat returns the "chat" Iconic.
func Chat() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>chat</title>
  <path d="M0 0v5l1-1h1v-3h3v-1h-5zm3 2v4h4l1 1v-5h-5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Check returns the "check" Iconic.
func Check() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>check</title>
  <path d="M6.41 0l-.69.72-2.78 2.78-.81-.78-.72-.72-1.41 1.41.72.72 1.5 1.5.69.72.72-.72 3.5-3.5.72-.72-1.44-1.41z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ChevronBottom returns the "chevron-bottom" Iconic.
func ChevronBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>chevron-bottom</title>
  <path d="M1.5 0l-1.5 1.5 4 4 4-4-1.5-1.5-2.5 2.5-2.5-2.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ChevronLeft returns the "chevron-left" Iconic.
func ChevronLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>chevron-left</title>
  <path d="M4 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ChevronRight returns the "chevron-right" Iconic.
func ChevronRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>chevron-right</title>
  <path d="M1.5 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ChevronTop returns the "chevron-top" Iconic.
func ChevronTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>chevron-top</title>
  <path d="M4 0l-4 4 1.5 1.5 2.5-2.5 2.5 2.5 1.5-1.5-4-4z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CircleCheck returns the "circle-check" Iconic.
func CircleCheck() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>circle-check</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm2 1.78l.72.72-3.22 3.22-1.72-1.72.72-.72 1 1 2.5-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CircleX returns the "circle-x" Iconic.
func CircleX() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>circle-x</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm-1.5 1.78l1.5 1.5 1.5-1.5.72.72-1.5 1.5 1.5 1.5-.72.72-1.5-1.5-1.5 1.5-.72-.72 1.5-1.5-1.5-1.5.72-.72z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Clipboard returns the "clipboard" Iconic.
func Clipboard() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>clipboard</title>
  <path d="M3.5 0c-.28 0-.5.22-.5.5v.5h-.75c-.14 0-.25.11-.25.25v.75h3v-.75c0-.14-.11-.25-.25-.25h-.75v-.5c0-.28-.22-.5-.5-.5zm-3.25 1c-.14 0-.25.11-.25.25v6.5c0 .14.11.25.25.25h6.5c.14 0 .25-.11.25-.25v-6.5c0-.14-.11-.25-.25-.25h-.75v2h-5v-2h-.75z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Clock returns the "clock" Iconic.
func Clock() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>clock</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm-.5 1v2.22l.16.13.5.5.34.38.72-.72-.38-.34-.34-.34v-1.81h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Cloud returns the "cloud" Iconic.
func Cloud() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cloud</title>
  <path d="M4.5 0c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2s.9 2 2 2h4.5c.83 0 1.5-.67 1.5-1.5 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CloudDownload returns the "cloud-download" Iconic.
func CloudDownload() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cloud-download</title>
  <path d="M4.5 0c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2 0 .37.11.71.28 1h2.72v-.5c0-.83.67-1.5 1.5-1.5s1.5.67 1.5 1.5v.5h1.91c.06-.16.09-.32.09-.5 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5zm-.16 4a.5.5 0 0 0-.34.5v1.5h-1.5l2 2 2-2h-1.5v-1.5a.5.5 0 0 0-.59-.5.5.5 0 0 0-.06 0z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CloudUpload returns the "cloud-upload" Iconic.
func CloudUpload() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cloud-upload</title>
  <path d="M4.5 0c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2 0 .37.11.71.28 1h2.22l2-2 2 2h1.41c.06-.16.09-.32.09-.5 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5zm0 4.5l-2.5 2.5h2v.5a.5.5 0 1 0 1 0v-.5h2l-2.5-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Cloudy returns the "cloudy" Iconic.
func Cloudy() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cloudy</title>
  <path d="M2.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .39.09.74.25 1.06.3-.21.64-.37 1-.47.55-1.25 1.82-2.09 3.25-2.09-.46-.6-1.18-1-2-1zm2 2c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2s.9 2 2 2h4.5c.83 0 1.5-.67 1.5-1.5 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Code returns the "code" Iconic.
func Code() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>code</title>
  <path d="M5 0l-3 6h1l3-6h-1zm-4 1l-1 2 1 2h1l-1-2 1-2h-1zm5 0l1 2-1 2h1l1-2-1-2h-1z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Cog returns the "cog" Iconic.
func Cog() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>cog</title>
  <path d="M3.5 0l-.5 1.19c-.1.03-.19.08-.28.13l-1.19-.5-.72.72.5 1.19c-.05.1-.09.18-.13.28l-1.19.5v1l1.19.5c.04.1.08.18.13.28l-.5 1.19.72.72 1.19-.5c.09.04.18.09.28.13l.5 1.19h1l.5-1.19c.09-.04.19-.08.28-.13l1.19.5.72-.72-.5-1.19c.04-.09.09-.19.13-.28l1.19-.5v-1l-1.19-.5c-.03-.09-.08-.19-.13-.28l.5-1.19-.72-.72-1.19.5c-.09-.04-.19-.09-.28-.13l-.5-1.19h-1zm.5 2.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5-1.5-.67-1.5-1.5.67-1.5 1.5-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CollapseDown returns the "collapse-down" Iconic.
func CollapseDown() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>collapse-down</title>
  <path d="M0 0v2h8v-2h-8zm2 3l2 2 2-2h-4zm-2 4v1h8v-1h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CollapseLeft returns the "collapse-left" Iconic.
func CollapseLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>collapse-left</title>
  <path d="M0 0v8h1v-8h-1zm6 0v8h2v-8h-2zm-1 2l-2 2 2 2v-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CollapseRight returns the "collapse-right" Iconic.
func CollapseRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>collapse-right</title>
  <path d="M0 0v8h2v-8h-2zm7 0v8h1v-8h-1zm-4 2v4l2-2-2-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CollapseUp returns the "collapse-up" Iconic.
func CollapseUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>collapse-up</title>
  <path d="M0 0v1h8v-1h-8zm4 3l-2 2h4l-2-2zm-4 3v2h8v-2h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Command returns the "command" Iconic.
func Command() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>command</title>
  <path d="M1.5 0c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5h.5v1h-.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5v-.5h1v.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5-.67-1.5-1.5-1.5h-.5v-1h.5c.83 0 1.5-.67 1.5-1.5s-.67-1.5-1.5-1.5-1.5.67-1.5 1.5v.5h-1v-.5c0-.83-.67-1.5-1.5-1.5zm0 1c.28 0 .5.22.5.5v.5h-.5c-.28 0-.5-.22-.5-.5s.22-.5.5-.5zm4 0c.28 0 .5.22.5.5s-.22.5-.5.5h-.5v-.5c0-.28.22-.5.5-.5zm-2.5 2h1v1h-1v-1zm-1.5 2h.5v.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5.22-.5.5-.5zm3.5 0h.5c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5v-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CommentSquare returns the "comment-square" Iconic.
func CommentSquare() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>comment-square</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v5.81c0 .05.04.09.09.09h5.91l2 2v-7.91c0-.06-.04-.09-.09-.09h-7.81z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Compass returns the "compass" Iconic.
func Compass() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>compass</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm2 1l-3 1-1 3 3-1 1-3zm-2 1.5c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Contrast returns the "contrast" Iconic.
func Contrast() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>contrast</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c1.66 0 3 1.34 3 3s-1.34 3-3 3v-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Copywriting returns the "copywriting" Iconic.
func Copywriting() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>copywriting</title>
  <path d="M0 0v1h8v-1h-8zm0 2v1h5v-1h-5zm0 3v1h8v-1h-8zm0 2v1h6v-1h-6zm7.5 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// CreditCard returns the "credit-card" Iconic.
func CreditCard() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>credit-card</title>
  <path d="M.25 0c-.14 0-.25.11-.25.25v.75h8v-.75c0-.14-.11-.25-.25-.25h-7.5zm-.25 2v3.75c0 .14.11.25.25.25h7.5c.14 0 .25-.11.25-.25v-3.75h-8zm1 2h1v1h-1v-1zm2 0h1v1h-1v-1z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Crop returns the "crop" Iconic.
func Crop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>crop</title>
  <path d="M1 0v1h-1v1h1v5h5v1h1v-1h1v-1h-1v-4.5l1-1-.5-.5-1 1h-4.5v-1h-1zm1 2h3.5l-3.5 3.5v-3.5zm4 .5v3.5h-3.5l3.5-3.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Dashboard returns the "dashboard" Iconic.
func Dashboard() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>dashboard</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 1c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm-1.66 1a.5.5 0 0 0-.19.84l.91.91c-.02.08-.06.16-.06.25 0 .55.45 1 1 1s1-.45 1-1-.45-1-1-1c-.09 0-.17.04-.25.06l-.91-.91a.5.5 0 0 0-.44-.16.5.5 0 0 0-.06 0zm3.16 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DataTransferDownload returns the "data-transfer-download" Iconic.
func DataTransferDownload() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>data-transfer-download</title>
  <path d="M3 0v3h-2l3 3 3-3h-2v-3h-2zm-3 7v1h8v-1h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DataTransferUpload returns the "data-transfer-upload" Iconic.
func DataTransferUpload() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>data-transfer-upload</title>
  <path d="M0 0v1h8v-1h-8zm4 2l-3 3h2v3h2v-3h2l-3-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Delete returns the "delete" Iconic.
func Delete() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>delete</title>
  <path d="M2 0l-2 3 2 3h6v-6h-6zm1.5.78l1.5 1.5 1.5-1.5.72.72-1.5 1.5 1.5 1.5-.72.72-1.5-1.5-1.5 1.5-.72-.72 1.5-1.5-1.5-1.5.72-.72z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Dial returns the "dial" Iconic.
func Dial() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>dial</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4h1c0-1.66 1.34-3 3-3s3 1.34 3 3h1c0-2.2-1.8-4-4-4zm-.59 2.09c-.81.25-1.41 1.01-1.41 1.91 0 1.11.9 2 2 2 1.11 0 2-.89 2-2 0-.9-.59-1.65-1.41-1.91l-.59.88-.59-.88z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Document returns the "document" Iconic.
func Document() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>document</title>
  <path d="M0 0v8h7v-4h-4v-4h-3zm4 0v3h3l-3-3zm-3 2h1v1h-1v-1zm0 2h1v1h-1v-1zm0 2h4v1h-4v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Dollar returns the "dollar" Iconic.
func Dollar() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>dollar</title>
  <path d="M2 0v1h-.75c-.68 0-1.25.57-1.25 1.25v.5c0 .68.44 1.24 1.09 1.41l2.56.66c.14.04.34.29.34.44v.5c0 .14-.11.25-.25.25h-2.5c-.12 0-.21-.04-.25-.06v-.94h-1v1c0 .34.2.63.44.78.23.16.52.22.81.22h.75v1h1v-1h.75c.69 0 1.25-.56 1.25-1.25v-.5c0-.68-.44-1.24-1.09-1.41l-2.56-.66c-.14-.04-.34-.29-.34-.44v-.5c0-.14.11-.25.25-.25h2.5c.11 0 .21.04.25.06v.94h1v-1c0-.34-.2-.63-.44-.78-.23-.16-.52-.22-.81-.22h-.75v-1h-1z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DoubleQuoteSansLeft returns the "double-quote-sans-left" Iconic.
func DoubleQuoteSansLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>double-quote-sans-left</title>
  <path d="M0 0v6l3-3v-3h-3zm5 0v6l3-3v-3h-3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DoubleQuoteSansRight returns the "double-quote-sans-right" Iconic.
func DoubleQuoteSansRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>double-quote-sans-right</title>
  <path d="M3 0l-3 3v3h3v-6zm5 0l-3 3v3h3v-6z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DoubleQuoteSerifLeft returns the "double-quote-serif-left" Iconic.
func DoubleQuoteSerifLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>double-quote-serif-left</title>
  <path d="M3 0c-1.65 0-3 1.35-3 3v3h3v-3h-2c0-1.11.89-2 2-2v-1zm5 0c-1.65 0-3 1.35-3 3v3h3v-3h-2c0-1.11.89-2 2-2v-1z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// DoubleQuoteSerifRight returns the "double-quote-serif-right" Iconic.
func DoubleQuoteSerifRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>double-quote-serif-right</title>
  <path d="M0 0v3h2c0 1.11-.89 2-2 2v1c1.65 0 3-1.35 3-3v-3h-3zm5 0v3h2c0 1.11-.89 2-2 2v1c1.65 0 3-1.35 3-3v-3h-3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Droplet returns the "droplet" Iconic.
func Droplet() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>droplet</title>
  <path d="M3 0l-.34.34c-.11.11-2.66 2.69-2.66 4.88 0 1.65 1.35 3 3 3s3-1.35 3-3c0-2.18-2.55-4.77-2.66-4.88l-.34-.34zm-1.5 4.72c.28 0 .5.22.5.5 0 .55.45 1 1 1 .28 0 .5.22.5.5s-.22.5-.5.5c-1.1 0-2-.9-2-2 0-.28.22-.5.5-.5z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Eject returns the "eject" Iconic.
func Eject() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>eject</title>
  <path d="M4 0l-4 5h8l-4-5zm-4 6v2h8v-2h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Elevator returns the "elevator" Iconic.
func Elevator() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>elevator</title>
  <path d="M3 0l-3 3h6l-3-3zm-3 5l3 3 3-3h-6z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Ellipses returns the "ellipses" Iconic.
func Ellipses() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>ellipses</title>
  <path d="M0 0v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2z" transform="translate(0 3)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// EnvelopeClosed returns the "envelope-closed" Iconic.
func EnvelopeClosed() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>envelope-closed</title>
  <path d="M0 0v1l4 2 4-2v-1h-8zm0 2v4h8v-4l-4 2-4-2z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// EnvelopeOpen returns the "envelope-open" Iconic.
func EnvelopeOpen() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>envelope-open</title>
  <path d="M4 0l-4 2v6h8v-6l-4-2zm0 1.13l3 1.5v1.88l-3 1.5-3-1.5v-1.88l3-1.5zm-2 1.88v1l2 1 2-1v-1h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Euro returns the "euro" Iconic.
func Euro() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>euro</title>
  <path d="M6 0c-1.86 0-3.4 1.28-3.84 3h-1.91l-.25 1h2.01c0 .35.07.68.16 1h-1.97l-.19 1h2.56c.7 1.19 1.97 2 3.44 2 .73 0 1.41-.21 2-.56v-1.22c-.53.48-1.22.78-2 .78-.89 0-1.67-.39-2.22-1h2.22l.16-1h-2.97c-.11-.32-.19-.64-.19-1h3.34l.16-1h-3.31c.41-1.16 1.51-2 2.81-2 .66 0 1.26.21 1.75.56l.16-1.06c-.57-.31-1.21-.5-1.91-.5z" transform="translate(-1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Excerpt returns the "excerpt" Iconic.
func Excerpt() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>excerpt</title>
  <path d="M0 0v1h7v-1h-7zm0 2v1h5v-1h-5zm0 2v1h8v-1h-8zm0 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ExpandDown returns the "expand-down" Iconic.
func ExpandDown() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>expand-down</title>
  <path d="M0 0v1h8v-1h-8zm2 2l2 2 2-2h-4zm-2 4v2h8v-2h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ExpandLeft returns the "expand-left" Iconic.
func ExpandLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>expand-left</title>
  <path d="M0 0v8h1v-8h-1zm6 0v8h2v-8h-2zm-4 2v4l2-2-2-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ExpandRight returns the "expand-right" Iconic.
func ExpandRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>expand-right</title>
  <path d="M0 0v8h2v-8h-2zm7 0v8h1v-8h-1zm-1 2l-2 2 2 2v-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ExpandUp returns the "expand-up" Iconic.
func ExpandUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>expand-up</title>
  <path d="M0 0v2h8v-2h-8zm4 4l-2 2h4l-2-2zm-4 3v1h8v-1h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ExternalLink returns the "external-link" Iconic.
func ExternalLink() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>external-link</title>
  <path d="M0 0v8h8v-2h-1v1h-6v-6h1v-1h-2zm4 0l1.5 1.5-2.5 2.5 1 1 2.5-2.5 1.5 1.5v-4h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Eye returns the "eye" Iconic.
func Eye() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>eye</title>
  <path d="M4.03 0c-2.53 0-4.03 3-4.03 3s1.5 3 4.03 3c2.47 0 3.97-3 3.97-3s-1.5-3-3.97-3zm-.03 1c1.11 0 2 .9 2 2 0 1.11-.89 2-2 2-1.1 0-2-.89-2-2 0-1.1.9-2 2-2zm0 1c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1c0-.1-.04-.19-.06-.28-.08.16-.24.28-.44.28-.28 0-.5-.22-.5-.5 0-.2.12-.36.28-.44-.09-.03-.18-.06-.28-.06z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Eyedropper returns the "eyedropper" Iconic.
func Eyedropper() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>eyedropper</title>
  <path d="M3.31 0a.5.5 0 0 0-.19.84l.63.63-3.59 3.66-.16.16v2.7199999999999998h2.69l.16-.16 3.66-3.66.63.66a.5.5 0 1 0 .72-.69l-.94-.94.66-.66c.59-.58.59-1.54 0-2.13-.57-.57-1.56-.57-2.13 0l-.66.66-.94-.94a.5.5 0 0 0-.47-.16.5.5 0 0 0-.06 0zm1.16 2.19l1.31 1.31-3.16 3.16-1.28-1.31 3.13-3.16z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// File returns the "file" Iconic.
func File() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>file</title>
  <path d="M0 0v8h7v-4h-4v-4h-3zm4 0v3h3l-3-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Fire returns the "fire" Iconic.
func Fire() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>fire</title>
  <path d="M2 0c1 2-2 3-2 5s2 3 2 3c-.98-1.98 2-3 2-5s-2-3-2-3zm3 3c1 2-2 3-2 5h3c.4 0 1-.5 1-2 0-2-2-3-2-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Flag returns the "flag" Iconic.
func Flag() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>flag</title>
  <path d="M0 0v8h1v-8h-1zm2 0v4h2v1h4l-2-1.97 2-2.03h-3v-1h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Flash returns the "flash" Iconic.
func Flash() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>flash</title>
  <path d="M1.5 0l-1.5 3h2l-.66 2h-1.34l1 3 3-3h-1.5l1.5-3h-2l1-2h-1.5z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Folder returns the "folder" Iconic.
func Folder() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>folder</title>
  <path d="M0 0v2h8v-1h-5v-1h-3zm0 3v4.5c0 .28.22.5.5.5h7c.28 0 .5-.22.5-.5v-4.5h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Fork returns the "fork" Iconic.
func Fork() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>fork</title>
  <path d="M1.5 0c-.83 0-1.5.67-1.5 1.5 0 .66.41 1.2 1 1.41v2.19c-.59.2-1 .75-1 1.41 0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5c0-.6-.34-1.1-.84-1.34.09-.09.21-.16.34-.16h2c.82 0 1.5-.68 1.5-1.5v-.59c.59-.2 1-.75 1-1.41 0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5c0 .66.41 1.2 1 1.41v.59c0 .28-.22.5-.5.5h-2c-.17 0-.35.04-.5.09v-1.19c.59-.2 1-.75 1-1.41 0-.83-.67-1.5-1.5-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// FullscreenEnter returns the "fullscreen-enter" Iconic.
func FullscreenEnter() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>fullscreen-enter</title>
  <path d="M0 0v4l1.5-1.5 1.5 1.5 1-1-1.5-1.5 1.5-1.5h-4zm5 4l-1 1 1.5 1.5-1.5 1.5h4v-4l-1.5 1.5-1.5-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// FullscreenExit returns the "fullscreen-exit" Iconic.
func FullscreenExit() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>fullscreen-exit</title>
  <path d="M1 0l-1 1 1.5 1.5-1.5 1.5h4v-4l-1.5 1.5-1.5-1.5zm3 4v4l1.5-1.5 1.5 1.5 1-1-1.5-1.5 1.5-1.5h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Globe returns the "globe" Iconic.
func Globe() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>globe</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1c.33 0 .64.09.94.19-.21.2-.45.38-.41.56.04.18.69.13.69.5 0 .27-.42.35-.13.66.35.35-.64.98-.66 1.44-.03.83.84.97 1.53.97.42 0 .53.2.5.44-.54.77-1.46 1.25-2.47 1.25-.38 0-.73-.09-1.06-.22.22-.44-.28-1.31-.75-1.59-.23-.23-.72-.14-1-.25-.09-.27-.18-.54-.19-.84.03-.05.08-.09.16-.09.19 0 .45.38.59.34.18-.04-.74-1.31-.31-1.56.2-.12.6.39.47-.16-.12-.51.36-.28.66-.41.26-.11.45-.41.13-.59-.06-.03-.13-.1-.22-.19.45-.27.97-.44 1.53-.44zm2.31 1.09c.18.22.32.46.44.72 0 .01 0 .02 0 .03-.04.07-.11.11-.22.22-.28.28-.32-.21-.44-.31-.13-.12-.6.02-.66-.13-.07-.18.5-.42.88-.53z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Graph returns the "graph" Iconic.
func Graph() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>graph</title>
  <path d="M7.03 0l-3.03 3-1-1-3 3.03 1 1 2-2.03 1 1 4-4-.97-1zm-7.03 7v1h8v-1h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// GridFourUp returns the "grid-four-up" Iconic.
func GridFourUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>grid-four-up</title>
  <path d="M0 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm-6 2v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1zm2 0v1h1v-1h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// GridThreeUp returns the "grid-three-up" Iconic.
func GridThreeUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>grid-three-up</title>
  <path d="M0 0v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 3v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 3v2h2v-2h-2zm3 0v2h2v-2h-2zm3 0v2h2v-2h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// GridTwoUp returns the "grid-two-up" Iconic.
func GridTwoUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>grid-two-up</title>
  <path d="M0 0v3h3v-3h-3zm5 0v3h3v-3h-3zm-5 5v3h3v-3h-3zm5 0v3h3v-3h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// HardDrive returns the "hard-drive" Iconic.
func HardDrive() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>hard-drive</title>
  <path d="M.19 0c-.11 0-.19.08-.19.19v3.31c0 .28.22.5.5.5h6c.28 0 .5-.22.5-.5v-3.31c0-.11-.08-.19-.19-.19h-6.63zm-.19 4.91v2.91c0 .11.08.19.19.19h6.63c.11 0 .19-.08.19-.19v-2.91c-.16.06-.32.09-.5.09h-6c-.18 0-.34-.04-.5-.09zm5.5 1.09c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Header returns the "header" Iconic.
func Header() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>header</title>
  <path d="M0 0v1h.5c.28 0 .5.22.5.5v4c0 .28-.22.5-.5.5h-.5v1h3v-1h-.5c-.28 0-.5-.22-.5-.5v-1.5h3v1.5c0 .28-.22.5-.5.5h-.5v1h3v-1h-.5c-.28 0-.5-.22-.5-.5v-4c0-.28.22-.5.5-.5h.5v-1h-3v1h.5c.28 0 .5.22.5.5v1.5h-3v-1.5c0-.28.22-.5.5-.5h.5v-1h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Headphones returns the "headphones" Iconic.
func Headphones() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>headphones</title>
  <path d="M4 0c-1.65 0-3 1.35-3 3v1h-.5a.5.5 0 0 0-.5.5v2a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-3.5c0-1.11.89-2 2-2 1.11 0 2 .89 2 2v3.5a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-2a.5.5 0 0 0-.5-.5h-.5v-1c0-1.65-1.35-3-3-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Heart returns the "heart" Iconic.
func Heart() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>heart</title>
  <path d="M2 0c-.55 0-1.04.23-1.41.59-.36.36-.59.85-.59 1.41 0 .55.23 1.04.59 1.41l3.41 3.41 3.41-3.41c.36-.36.59-.85.59-1.41 0-.55-.23-1.04-.59-1.41-.36-.36-.85-.59-1.41-.59-.55 0-1.04.23-1.41.59-.36.36-.59.85-.59 1.41 0-.55-.23-1.04-.59-1.41-.36-.36-.85-.59-1.41-.59z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Home returns the "home" Iconic.
func Home() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>home</title>
  <path d="M4 0l-4 3h1v4h2v-2h2v2h2v-4.03l1 .03-4-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Image returns the "image" Iconic.
func Image() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>image</title>
  <path d="M0 0v8h8v-8h-8zm1 1h6v3l-1-1-1 1 2 2v1h-1l-4-4-1 1v-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Inbox returns the "inbox" Iconic.
func Inbox() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>inbox</title>
  <path d="M.19 0c-.11 0-.19.08-.19.19v7.63c0 .11.08.19.19.19h7.63c.11 0 .19-.08.19-.19v-7.63c0-.11-.08-.19-.19-.19h-7.63zm.81 2h6v3h-1l-1 1h-2l-1-1h-1v-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Infinity returns the "infinity" Iconic.
func Infinity() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>infinity</title>
  <path d="M2 0c-1.31 0-2 1.01-2 2s.69 2 2 2c.79 0 1.42-.56 2-1.22.58.66 1.19 1.22 2 1.22 1.31 0 2-1.01 2-2s-.69-2-2-2c-.81 0-1.42.56-2 1.22-.58-.66-1.21-1.22-2-1.22zm0 1c.42 0 .88.47 1.34 1-.46.53-.92 1-1.34 1-.74 0-1-.54-1-1 0-.46.26-1 1-1zm4 0c.74 0 1 .54 1 1 0 .46-.26 1-1 1-.43 0-.89-.47-1.34-1 .46-.53.91-1 1.34-1z" transform="translate(0 2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Info returns the "info" Iconic.
func Info() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>info</title>
  <path d="M3 0c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1zm-1.5 2.5c-.83 0-1.5.67-1.5 1.5h1c0-.28.22-.5.5-.5s.5.22.5.5-1 1.64-1 2.5c0 .86.67 1.5 1.5 1.5s1.5-.67 1.5-1.5h-1c0 .28-.22.5-.5.5s-.5-.22-.5-.5c0-.36 1-1.84 1-2.5 0-.81-.67-1.5-1.5-1.5z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Italic returns the "italic" Iconic.
func Italic() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>italic</title>
  <path d="M2 0v1h1.63l-.06.13-2 5-.34.88h-1.22v1h5v-1h-1.63l.06-.13 2-5 .34-.88h1.22v-1h-5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// JustifyCenter returns the "justify-center" Iconic.
func JustifyCenter() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>justify-center</title>
  <path d="M0 0v1h8v-1h-8zm0 2v1h8v-1h-8zm0 2v1h8v-1h-8zm1 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// JustifyLeft returns the "justify-left" Iconic.
func JustifyLeft() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>justify-left</title>
  <path d="M0 0v1h8v-1h-8zm0 2v1h8v-1h-8zm0 2v1h8v-1h-8zm0 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// JustifyRight returns the "justify-right" Iconic.
func JustifyRight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>justify-right</title>
  <path d="M0 0v1h8v-1h-8zm0 2v1h8v-1h-8zm0 2v1h8v-1h-8zm2 2v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Key returns the "key" Iconic.
func Key() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>key</title>
  <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .16 0 .32.03.47l-3.03 3.03v2h3v-2h2v-1l.03-.03c.15.03.31.03.47.03 1.38 0 2.5-1.12 2.5-2.5s-1.12-2.5-2.5-2.5zm.5 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Laptop returns the "laptop" Iconic.
func Laptop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>laptop</title>
  <path d="M1.34 0a.5.5 0 0 0-.34.5v3.5h-1v1.5c0 .28.22.5.5.5h7.010000000000001c.28 0 .5-.22.5-.5v-1.5h-1v-3.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0zm.66 1h4v3h-1v1h-2v-1h-1v-3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Layers returns the "layers" Iconic.
func Layers() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>layers</title>
  <path d="M0 0v4h4v-4h-4zm5 2v3h-3v1h4v-4h-1zm2 2v3h-3v1h4v-4h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Lightbulb returns the "lightbulb" Iconic.
func Lightbulb() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>lightbulb</title>
  <path d="M3.41 0a.5.5 0 0 0-.13.06l-3 1.5a.5.5 0 1 0 .44.88l3-1.5a.5.5 0 0 0-.31-.94zm1 1.5a.5.5 0 0 0-.13.06l-4 2a.5.5 0 1 0 .44.88l4-2a.5.5 0 0 0-.31-.94zm0 2a.5.5 0 0 0-.13.06l-3 1.5a.5.5 0 0 0 .22.94h2a.5.5 0 0 0 .16-1l1.06-.56a.5.5 0 0 0-.31-.94zm-2.56 3.5a.5.5 0 0 0 .16 1h1a.5.5 0 1 0 0-1h-1a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LinkBroken returns the "link-broken" Iconic.
func LinkBroken() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>link-broken</title>
  <path d="M2 0v1h-1v1h2v-2h-1zm3.88.03c-.18.01-.36.03-.53.09-.27.1-.53.25-.75.47l-.44.44a.5.5 0 1 0 .69.69l.44-.44c.11-.11.24-.17.38-.22.35-.12.78-.07 1.06.22.39.39.39 1.04 0 1.44l-1.5 1.5a.5.5 0 1 0 .69.69l1.5-1.5c.78-.78.78-2.04 0-2.81-.28-.28-.61-.45-.97-.53-.18-.04-.38-.04-.56-.03zm-3.59 2.91a.5.5 0 0 0-.19.16l-1.5 1.5c-.78.78-.78 2.04 0 2.81.56.56 1.36.72 2.06.47.27-.1.53-.25.75-.47l.44-.44a.5.5 0 1 0-.69-.69l-.44.44c-.11.11-.24.17-.38.22-.35.12-.78.07-1.06-.22-.39-.39-.39-1.04 0-1.44l1.5-1.5a.5.5 0 0 0-.44-.84.5.5 0 0 0-.06 0zm2.72 3.06v2h1v-1h1v-1h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LinkIntact returns the "link-intact" Iconic.
func LinkIntact() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>link-intact</title>
  <path d="M5.88.03c-.18.01-.36.03-.53.09-.27.1-.53.25-.75.47a.5.5 0 1 0 .69.69c.11-.11.24-.17.38-.22.35-.12.78-.07 1.06.22.39.39.39 1.04 0 1.44l-1.5 1.5c-.44.44-.8.48-1.06.47-.26-.01-.41-.13-.41-.13a.5.5 0 1 0-.5.88s.34.22.84.25c.5.03 1.2-.16 1.81-.78l1.5-1.5c.78-.78.78-2.04 0-2.81-.28-.28-.61-.45-.97-.53-.18-.04-.38-.04-.56-.03zm-2 2.31c-.5-.02-1.19.15-1.78.75l-1.5 1.5c-.78.78-.78 2.04 0 2.81.56.56 1.36.72 2.06.47.27-.1.53-.25.75-.47a.5.5 0 1 0-.69-.69c-.11.11-.24.17-.38.22-.35.12-.78.07-1.06-.22-.39-.39-.39-1.04 0-1.44l1.5-1.5c.4-.4.75-.45 1.03-.44.28.01.47.09.47.09a.5.5 0 1 0 .44-.88s-.34-.2-.84-.22z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// List returns the "list" Iconic.
func List() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>list</title>
  <path d="M.5 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm1.5 0v1h6v-1h-6zm-1.5 2c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm1.5 0v1h6v-1h-6zm-1.5 2c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm1.5 0v1h6v-1h-6zm-1.5 2c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm1.5 0v1h6v-1h-6z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ListRich returns the "list-rich" Iconic.
func ListRich() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>list-rich</title>
  <path d="M0 0v3h3v-3h-3zm4 0v1h4v-1h-4zm0 2v1h3v-1h-3zm-4 2v3h3v-3h-3zm4 0v1h4v-1h-4zm0 2v1h3v-1h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Location returns the "location" Iconic.
func Location() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>location</title>
  <path d="M8 0l-8 4 3 1 1 3 4-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LockLocked returns the "lock-locked" Iconic.
func LockLocked() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>lock-locked</title>
  <path d="M3 0c-1.1 0-2 .9-2 2v1h-1v4h6v-4h-1v-1c0-1.1-.9-2-2-2zm0 1c.56 0 1 .44 1 1v1h-2v-1c0-.56.44-1 1-1z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LockUnlocked returns the "lock-unlocked" Iconic.
func LockUnlocked() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>lock-unlocked</title>
  <path d="M3 0c-1.1 0-2 .9-2 2h1c0-.56.44-1 1-1s1 .44 1 1v2h-4v4h6v-4h-1v-2c0-1.1-.9-2-2-2z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Loop returns the "loop" Iconic.
func Loop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>loop</title>
  <path d="M6 0v1h-5c-.55 0-1 .45-1 1v1h1v-1h5v1l2-1.5-2-1.5zm-4 4l-2 1.5 2 1.5v-1h5c.55 0 1-.45 1-1v-1h-1v1h-5v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LoopCircular returns the "loop-circular" Iconic.
func LoopCircular() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>loop-circular</title>
  <path d="M4 0c-1.65 0-3 1.35-3 3h-1l1.5 2 1.5-2h-1c0-1.11.89-2 2-2v-1zm2.5 1l-1.5 2h1c0 1.11-.89 2-2 2v1c1.65 0 3-1.35 3-3h1l-1.5-2z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// LoopSquare returns the "loop-square" Iconic.
func LoopSquare() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>loop-square</title>
  <path d="M1 0v2h1v-1h4v2h-1l1.5 2.5 1.5-2.5h-1v-3h-6zm.5 2.5l-1.5 2.5h1v3h6v-2h-1v1h-4v-2h1l-1.5-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MagnifyingGlass returns the "magnifying-glass" Iconic.
func MagnifyingGlass() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>magnifying-glass</title>
  <path d="M3.5 0c-1.93 0-3.5 1.57-3.5 3.5s1.57 3.5 3.5 3.5c.59 0 1.17-.14 1.66-.41a1 1 0 0 0 .13.13l1 1a1.02 1.02 0 1 0 1.44-1.44l-1-1a1 1 0 0 0-.16-.13c.27-.49.44-1.06.44-1.66 0-1.93-1.57-3.5-3.5-3.5zm0 1c1.39 0 2.5 1.11 2.5 2.5 0 .66-.24 1.27-.66 1.72-.01.01-.02.02-.03.03a1 1 0 0 0-.13.13c-.44.4-1.04.63-1.69.63-1.39 0-2.5-1.11-2.5-2.5s1.11-2.5 2.5-2.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Map returns the "map" Iconic.
func Map() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>map</title>
  <path d="M0 0v8h8v-2.38a.5.5 0 0 0 0-.22v-5.41h-8zm1 1h6v4h-1.5a.5.5 0 0 0-.09 0 .5.5 0 1 0 .09 1h1.5v1h-6v-6zm2.5 1c-.83 0-1.5.67-1.5 1.5 0 1 1.5 2.5 1.5 2.5s1.5-1.5 1.5-2.5c0-.83-.67-1.5-1.5-1.5zm0 1c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MapMarker returns the "map-marker" Iconic.
func MapMarker() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>map-marker</title>
  <path d="M3 0c-1.66 0-3 1.34-3 3 0 2 3 5 3 5s3-3 3-5c0-1.66-1.34-3-3-3zm0 1c1.11 0 2 .9 2 2 0 1.11-.89 2-2 2-1.1 0-2-.89-2-2 0-1.1.9-2 2-2z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaPause returns the "media-pause" Iconic.
func MediaPause() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-pause</title>
  <path d="M0 0v6h2v-6h-2zm4 0v6h2v-6h-2z" transform="translate(1 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaPlay returns the "media-play" Iconic.
func MediaPlay() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-play</title>
  <path d="M0 0v6l6-3-6-3z" transform="translate(1 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaRecord returns the "media-record" Iconic.
func MediaRecord() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-record</title>
  <path d="M3 0c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z" transform="translate(1 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaSkipBackward returns the "media-skip-backward" Iconic.
func MediaSkipBackward() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-skip-backward</title>
  <path d="M4 0l-4 3 4 3v-6zm0 3l4 3v-6l-4 3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaSkipForward returns the "media-skip-forward" Iconic.
func MediaSkipForward() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-skip-forward</title>
  <path d="M0 0v6l4-3-4-3zm4 3v3l4-3-4-3v3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaStepBackward returns the "media-step-backward" Iconic.
func MediaStepBackward() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-step-backward</title>
  <path d="M0 0v6h2v-6h-2zm2 3l5 3v-6l-5 3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaStepForward returns the "media-step-forward" Iconic.
func MediaStepForward() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-step-forward</title>
  <path d="M0 0v6l5-3-5-3zm5 3v3h2v-6h-2v3z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MediaStop returns the "media-stop" Iconic.
func MediaStop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>media-stop</title>
  <path d="M0 0v6h6v-6h-6z" transform="translate(1 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MedicalCross returns the "medical-cross" Iconic.
func MedicalCross() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>medical-cross</title>
  <path d="M2 0v2h-2v4h2v2h4v-2h2v-4h-2v-2h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Menu returns the "menu" Iconic.
func Menu() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>menu</title>
  <path d="M0 0v1h8v-1h-8zm0 2.97v1h8v-1h-8zm0 3v1h8v-1h-8z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Microphone returns the "microphone" Iconic.
func Microphone() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>microphone</title>
  <path d="M2.91-.03a1 1 0 0 0-.13.03 1 1 0 0 0-.78 1v2a1 1 0 1 0 2 0v-2a1 1 0 0 0-1.09-1.03zm-2.56 2.03a.5.5 0 0 0-.34.5v.5c0 1.48 1.09 2.69 2.5 2.94v1.06h-.5c-.55 0-1 .45-1 1h4.01c0-.55-.45-1-1-1h-.5v-1.06c1.41-.24 2.5-1.46 2.5-2.94v-.5a.5.5 0 1 0-1 0v.5c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2v-.5a.5.5 0 0 0-.59-.5.5.5 0 0 0-.06 0z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Minus returns the "minus" Iconic.
func Minus() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>minus</title>
  <path d="M0 0v2h8v-2h-8z" transform="translate(0 3)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Monitor returns the "monitor" Iconic.
func Monitor() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>monitor</title>
  <path d="M.34 0a.5.5 0 0 0-.34.5v5a.5.5 0 0 0 .5.5h2.5v1h-1c-.55 0-1 .45-1 1h6c0-.55-.45-1-1-1h-1v-1h2.5a.5.5 0 0 0 .5-.5v-5a.5.5 0 0 0-.5-.5h-7a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0zm.66 1h6v4h-6v-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Moon returns the "moon" Iconic.
func Moon() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>moon</title>
  <path d="M2.72 0c-1.58.53-2.72 2.02-2.72 3.78 0 2.21 1.79 4 4 4 1.76 0 3.25-1.14 3.78-2.72-.4.13-.83.22-1.28.22-2.21 0-4-1.79-4-4 0-.45.08-.88.22-1.28z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Move returns the "move" Iconic.
func Move() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>move</title>
  <path d="M3.5 0l-1.5 1.5h1v1.5h-1.5v-1l-1.5 1.5 1.5 1.5v-1h1.5v1.5h-1l1.5 1.5 1.5-1.5h-1v-1.5h1.5v1l1.5-1.5-1.5-1.5v1h-1.5v-1.5h1l-1.5-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// MusicalNote returns the "musical-note" Iconic.
func MusicalNote() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>musical-note</title>
  <path d="M8 0c-5 0-6 1-6 1v4.09c-.15-.05-.33-.09-.5-.09-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5v-3.97c.73-.23 1.99-.44 4-.5v2.06c-.15-.05-.33-.09-.5-.09-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5v-5.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Paperclip returns the "paperclip" Iconic.
func Paperclip() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>paperclip</title>
  <path d="M5 0c-.51 0-1.02.21-1.41.59l-2.78 2.72c-1.07 1.07-1.07 2.8 0 3.88 1.07 1.07 2.8 1.07 3.88 0l1.25-1.25-.69-.69-1.16 1.13-.09.13c-.69.69-1.81.69-2.5 0-.68-.68-.66-1.78 0-2.47l2.78-2.75c.39-.39 1.04-.39 1.44 0 .39.39.37 1.01 0 1.41l-2.5 2.47c-.1.1-.27.1-.38 0-.1-.1-.1-.27 0-.38l.06-.03.91-.94-.69-.69-.97.97c-.48.48-.48 1.27 0 1.75s1.27.49 1.75 0l2.5-2.44c.78-.78.78-2.04 0-2.81-.39-.39-.89-.59-1.41-.59z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Pencil returns the "pencil" Iconic.
func Pencil() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>pencil</title>
  <path d="M6 0l-1 1 2 2 1-1-2-2zm-2 2l-4 4v2h2l4-4-2-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// People returns the "people" Iconic.
func People() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>people</title>
  <path d="M5.5 0c-.51 0-.95.35-1.22.88.45.54.72 1.28.72 2.13 0 .29-.03.55-.09.81.19.11.38.19.59.19.83 0 1.5-.9 1.5-2s-.67-2-1.5-2zm-3 1c-.83 0-1.5.9-1.5 2s.67 2 1.5 2 1.5-.9 1.5-2-.67-2-1.5-2zm4.75 3.16c-.43.51-1.02.82-1.69.84.27.38.44.84.44 1.34v.66h2v-1.66c0-.52-.31-.97-.75-1.19zm-6.5 1c-.44.22-.75.67-.75 1.19v1.66h5v-1.66c0-.52-.31-.97-.75-1.19-.45.53-1.06.84-1.75.84s-1.3-.32-1.75-.84z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Person returns the "person" Iconic.
func Person() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>person</title>
  <path d="M4 0c-1.1 0-2 1.12-2 2.5s.9 2.5 2 2.5 2-1.12 2-2.5-.9-2.5-2-2.5zm-2.09 5c-1.06.05-1.91.92-1.91 2v1h8v-1c0-1.08-.84-1.95-1.91-2-.54.61-1.28 1-2.09 1-.81 0-1.55-.39-2.09-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Phone returns the "phone" Iconic.
func Phone() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>phone</title>
  <path d="M.19 0c-.11 0-.19.08-.19.19v7.63c0 .11.08.19.19.19h4.63c.11 0 .19-.08.19-.19v-7.63c0-.11-.08-.19-.19-.19h-4.63zm.81 1h3v5h-3v-5zm1.5 5.5c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// PieChart returns the "pie-chart" Iconic.
func PieChart() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>pie-chart</title>
  <path d="M3.5 0c-.97 0-1.84.4-2.47 1.03l2.97 2.97v-3.97c-.16-.02-.33-.03-.5-.03zm1.5 1.06v3.41l-2.72 2.72c.61.5 1.37.81 2.22.81 1.93 0 3.5-1.57 3.5-3.5 0-1.76-1.31-3.19-3-3.44zm-4.09 1.31c-.56.54-.91 1.29-.91 2.13 0 .96.46 1.79 1.16 2.34l2.13-2.13-2.38-2.34z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Pin returns the "pin" Iconic.
func Pin() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>pin</title>
  <path d="M1.34 0a.5.5 0 0 0 .16 1h.5v2h-1c-.55 0-1 .45-1 1h3v3l.44 1 .56-1v-3h3c0-.55-.45-1-1-1h-1v-2h.5a.5.5 0 1 0 0-1h-4a.5.5 0 0 0-.09 0 .5.5 0 0 0-.06 0z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// PlayCircle returns the "play-circle" Iconic.
func PlayCircle() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>play-circle</title>
  <path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm-1 2l3 2-3 2v-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Plus returns the "plus" Iconic.
func Plus() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>plus</title>
  <path d="M3 0v3h-3v2h3v3h2v-3h3v-2h-3v-3h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// PowerStandby returns the "power-standby" Iconic.
func PowerStandby() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>power-standby</title>
  <path d="M3 0v4h1v-4h-1zm-1.28 1.44l-.38.31c-.81.64-1.34 1.64-1.34 2.75 0 1.93 1.57 3.5 3.5 3.5s3.5-1.57 3.5-3.5c0-1.11-.53-2.11-1.34-2.75l-.38-.31-.63.78.38.31c.58.46.97 1.17.97 1.97 0 1.39-1.11 2.5-2.5 2.5s-2.5-1.11-2.5-2.5c0-.8.36-1.51.94-1.97l.41-.31-.63-.78z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Print returns the "print" Iconic.
func Print() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>print</title>
  <path d="M2 0v2h4v-2h-4zm-1.91 3c-.06 0-.09.04-.09.09v2.81c0 .05.04.09.09.09h.91v-2h6v2h.91c.05 0 .09-.04.09-.09v-2.81c0-.06-.04-.09-.09-.09h-7.81zm1.91 2v3h4v-3h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Project returns the "project" Iconic.
func Project() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>project</title>
  <path d="M0 0v7h1v-7h-1zm7 0v7h1v-7h-1zm-5 1v1h2v-1h-2zm1 2v1h2v-1h-2zm1 2v1h2v-1h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Pulse returns the "pulse" Iconic.
func Pulse() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>pulse</title>
  <path d="M3.25 0l-.47 1.53-.78 2.56-.03-.06-.09-.34h-1.88v1h1.1600000000000001l.38 1.16.47 1.47.47-1.5.78-2.5.78 2.5.41 1.34.53-1.28.59-1.47.13.28h2.31v-1h-1.69l-.38-.75-.5-.97-.41 1.03-.47 1.19-.84-2.66-.47-1.53z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// PuzzlePiece returns the "puzzle-piece" Iconic.
func PuzzlePiece() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>puzzle-piece</title>
  <path d="M3 0c-.28 0-.54.1-.72.28-.18.18-.28.44-.28.72 0 .28.18.48.28.72.03.06.03.16.03.28h-2.31v6h2.31c0-.12-.01-.22-.03-.28-.1-.24-.28-.44-.28-.72 0-.28.1-.54.28-.72.18-.18.44-.28.72-.28.28 0 .54.1.72.28.18.18.28.44.28.72 0 .28-.18.48-.28.72-.03.06-.03.16-.03.28h2.31v-2.31c.12 0 .22.01.28.03.24.1.44.28.72.28.28 0 .54-.1.72-.28.18-.18.28-.44.28-.72 0-.28-.1-.54-.28-.72-.18-.18-.44-.28-.72-.28-.28 0-.48.18-.72.28-.06.03-.16.03-.28.03v-2.31h-2.31c0-.12.01-.22.03-.28.1-.24.28-.44.28-.72 0-.28-.1-.54-.28-.72-.18-.18-.44-.28-.72-.28z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// QuestionMark returns the "question-mark" Iconic.
func QuestionMark() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>question-mark</title>
  <path d="M2.47 0c-.85 0-1.48.26-1.88.66-.4.4-.54.9-.59 1.28l1 .13c.04-.25.12-.5.31-.69.19-.19.49-.38 1.16-.38.66 0 1.02.16 1.22.34.2.18.28.4.28.66 0 .83-.34 1.06-.84 1.5-.5.44-1.16 1.08-1.16 2.25v.25h1v-.25c0-.83.31-1.06.81-1.5.5-.44 1.19-1.08 1.19-2.25 0-.48-.17-1.02-.59-1.41-.43-.39-1.07-.59-1.91-.59zm-.5 7v1h1v-1h-1z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Rain returns the "rain" Iconic.
func Rain() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>rain</title>
  <path d="M4.5 0c-1.21 0-2.27.86-2.5 2-1.1 0-2 .9-2 2 0 .53.2.99.53 1.34.26-.22.6-.34.97-.34.2 0 .39.05.56.13.17-.64.74-1.13 1.44-1.13.69 0 1.27.49 1.44 1.13.17-.07.36-.13.56-.13.63 0 1.15.39 1.38.94.64-.17 1.13-.75 1.13-1.44 0-.65-.42-1.29-1-1.5v-.5c0-1.38-1.12-2.5-2.5-2.5zm-1.16 5a.5.5 0 0 0-.34.5v2a.5.5 0 1 0 1 0v-2a.5.5 0 0 0-.59-.5.5.5 0 0 0-.06 0zm-2 1a.5.5 0 0 0-.34.5v1a.5.5 0 1 0 1 0v-1a.5.5 0 0 0-.59-.5.5.5 0 0 0-.06 0zm4 0a.5.5 0 0 0-.34.5v1a.5.5 0 1 0 1 0v-1a.5.5 0 0 0-.59-.5.5.5 0 0 0-.06 0z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Random returns the "random" Iconic.
func Random() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>random</title>
  <path d="M6 0v1h-.5c-.35 0-.56.1-.78.38l-1.41 1.78-1.53-1.78c-.22-.26-.44-.38-.78-.38h-1v1h1c-.05 0 .01.04.03.03l1.63 1.91-1.66 2.06h-1v1h1c.35 0 .56-.1.78-.38l1.53-1.91 1.66 1.91c.22.26.44.38.78.38h.25v1l2-1.5-2-1.5v1h-.22c-.01-.01-.05-.04-.06-.03l-1.75-2.06 1.53-1.91h.5v1l2-1.5-2-1.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Reload returns the "reload" Iconic.
func Reload() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>reload</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4c1.1 0 2.12-.43 2.84-1.16l-.72-.72c-.54.54-1.29.88-2.13.88-1.66 0-3-1.34-3-3s1.34-3 3-3c.83 0 1.55.36 2.09.91l-1.09 1.09h3v-3l-1.19 1.19c-.72-.72-1.71-1.19-2.81-1.19z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ResizeBoth returns the "resize-both" Iconic.
func ResizeBoth() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>resize-both</title>
  <path d="M4 0l1.66 1.66-4 4-1.66-1.66v4h4l-1.66-1.66 4-4 1.66 1.66v-4h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ResizeHeight returns the "resize-height" Iconic.
func ResizeHeight() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>resize-height</title>
  <path d="M2.5 0l-2.5 3h2v2h-2l2.5 3 2.5-3h-2v-2h2l-2.5-3z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ResizeWidth returns the "resize-width" Iconic.
func ResizeWidth() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>resize-width</title>
  <path d="M3 0l-3 2.5 3 2.5v-2h2v2l3-2.5-3-2.5v2h-2v-2z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Rss returns the "rss" Iconic.
func Rss() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>rss</title>
  <path d="M1 0v1c3.32 0 6 2.68 6 6h1c0-3.86-3.14-7-7-7zm0 2v1c2.21 0 4 1.79 4 4h1c0-2.76-2.24-5-5-5zm0 2v1c1.11 0 2 .89 2 2h1c0-1.65-1.35-3-3-3zm0 2c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// RssAlt returns the "rss-alt" Iconic.
func RssAlt() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>rss-alt</title>
  <path d="M0 0v2c3.33 0 6 2.67 6 6h2c0-4.41-3.59-8-8-8zm0 3v2c1.67 0 3 1.33 3 3h2c0-2.75-2.25-5-5-5zm0 3v2h2c0-1.11-.9-2-2-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Script returns the "script" Iconic.
func Script() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>script</title>
  <path d="M3 0c-.55 0-1 .45-1 1v5.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-1.5h-1v2c0 .55.45 1 1 1h5c.55 0 1-.45 1-1v-3h-4v-2.5c0-.28.22-.5.5-.5s.5.22.5.5v1.5h4v-2c0-.55-.45-1-1-1h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Share returns the "share" Iconic.
func Share() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>share</title>
  <path d="M5 0v2c-4 0-5 2.05-5 5 .52-1.98 2-3 4-3h1v2l3-3.16-3-2.84z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ShareBoxed returns the "share-boxed" Iconic.
func ShareBoxed() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>share-boxed</title>
  <path d="M.75 0c-.41 0-.75.34-.75.75v5.5c0 .41.34.75.75.75h4.5c.41 0 .75-.34.75-.75v-1.25h-1v1h-4v-5h2v-1h-2.25zm5.25 0v1c-2.05 0-3.7 1.54-3.94 3.53.21-.88.99-1.53 1.94-1.53h2v1l2-2-2-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Shield returns the "shield" Iconic.
func Shield() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>shield</title>
  <path d="M4 0l-.19.09-3.5 1.47-.31.13v.31c0 1.66.67 3.12 1.47 4.19.4.53.83.97 1.25 1.28.42.31.83.53 1.28.53.46 0 .86-.22 1.28-.53.42-.31.85-.75 1.25-1.28.8-1.07 1.47-2.53 1.47-4.19v-.31l-.31-.13-3.5-1.47-.19-.09zm0 1.09v5.91c-.04 0-.33-.07-.66-.31s-.71-.63-1.06-1.09c-.64-.85-1.14-2.03-1.22-3.28l2.94-1.22z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Signal returns the "signal" Iconic.
func Signal() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>signal</title>
  <path d="M6 0v8h1v-8h-1zm-2 1v7h1v-7h-1zm-2 2v5h1v-5h-1zm-2 2v3h1v-3h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Signpost returns the "signpost" Iconic.
func Signpost() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>signpost</title>
  <path d="M3 0v1h-2l-1 1 1 1h2v5h1v-4h2l1-1-1-1h-2v-2h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// SortAscending returns the "sort-ascending" Iconic.
func SortAscending() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>sort-ascending</title>
  <path d="M2 0v6h-2l2.5 2 2.5-2h-2v-6h-1zm2 0v1h2v-1h-2zm0 2v1h3v-1h-3zm0 2v1h4v-1h-4z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// SortDescending returns the "sort-descending" Iconic.
func SortDescending() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>sort-descending</title>
  <path d="M2 0v6h-2l2.5 2 2.5-2h-2v-6h-1zm2 0v1h4v-1h-4zm0 2v1h3v-1h-3zm0 2v1h2v-1h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Spreadsheet returns the "spreadsheet" Iconic.
func Spreadsheet() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>spreadsheet</title>
  <path d="M.75 0c-.41 0-.75.34-.75.75v5.5c0 .41.34.75.75.75h6.5c.41 0 .75-.34.75-.75v-5.5c0-.41-.34-.75-.75-.75h-6.5zm.25 1h1v1h-1v-1zm2 0h4v1h-4v-1zm-2 2h1v1h-1v-1zm2 0h4v1h-4v-1zm-2 2h1v1h-1v-1zm2 0h4v1h-4v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Star returns the "star" Iconic.
func Star() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>star</title>
  <path d="M4 0l-1 3h-3l2.5 2-1 3 2.5-2 2.5 2-1-3 2.5-2h-3l-1-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Sun returns the "sun" Iconic.
func Sun() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>sun</title>
  <path d="M4 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm-2.5 1c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm5 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm-2.5 1c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-3.5 1.5c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm7 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm-6 2.5c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm5 0c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5zm-2.5 1c-.28 0-.5.22-.5.5s.22.5.5.5.5-.22.5-.5-.22-.5-.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Tablet returns the "tablet" Iconic.
func Tablet() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>tablet</title>
  <path d="M.34 0c-.18 0-.34.16-.34.34v7.31c0 .18.16.34.34.34h6.31c.18 0 .34-.16.34-.34v-7.31c0-.18-.16-.34-.34-.34h-6.31zm.66 1h5v5h-5v-5zm2.5 5.5c.38 0 .63.42.44.75s-.68.33-.88 0c-.19-.33.05-.75.44-.75z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Tag returns the "tag" Iconic.
func Tag() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>tag</title>
  <path d="M0 0v3l5 5 3-3-5-5h-3zm2 1c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Tags returns the "tags" Iconic.
func Tags() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>tags</title>
  <path d="M0 0v2l3 3 1.5-1.5.5-.5-2-2-1-1h-2zm3.41 0l3 3-1.19 1.22.78.78 2-2-3-3h-1.59zm-1.91 1c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Target returns the "target" Iconic.
func Target() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>target</title>
  <path d="M4 0c-2.2 0-4 1.8-4 4s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4zm0 1c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 1c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 1c.56 0 1 .44 1 1s-.44 1-1 1-1-.44-1-1 .44-1 1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Task returns the "task" Iconic.
func Task() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>task</title>
  <path d="M0 0v7h7v-3.59l-1 1v1.59h-5v-5h3.59l1-1h-5.59zm7 0l-3 3-1-1-1 1 2 2 4-4-1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Terminal returns the "terminal" Iconic.
func Terminal() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>terminal</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v7.81c0 .05.04.09.09.09h7.81c.05 0 .09-.04.09-.09v-7.81c0-.06-.04-.09-.09-.09h-7.81zm1.41.78l1.72 1.72-1.72 1.72-.72-.72 1-1-1-1 .72-.72zm2.5 2.22h3v1h-3v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Text returns the "text" Iconic.
func Text() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>text</title>
  <path d="M0 0v2h.5c0-.55.45-1 1-1h1.5v5.5c0 .28-.22.5-.5.5h-.5v1h4v-1h-.5c-.28 0-.5-.22-.5-.5v-5.5h1.5c.55 0 1 .45 1 1h.5v-2h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ThumbDown returns the "thumb-down" Iconic.
func ThumbDown() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>thumb-down</title>
  <path d="M0 0v4h1v-4h-1zm2 0v4c.28 0 .53.09.72.28.19.19 1.15 2.12 1.28 2.38.13.26.39.39.66.31.26-.08.4-.36.31-.63-.08-.26-.47-1.59-.47-1.84s.22-.5.5-.5h1.5c.28 0 .5-.22.5-.5s-1.03-3.19-1.03-3.19c-.08-.18-.26-.31-.47-.31h-3.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ThumbUp returns the "thumb-up" Iconic.
func ThumbUp() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>thumb-up</title>
  <path d="M4.47 0c-.19.02-.37.15-.47.34-.13.26-1.09 2.19-1.28 2.38-.19.19-.44.28-.72.28v4h3.5c.21 0 .39-.13.47-.31 0 0 1.03-2.91 1.03-3.19 0-.28-.22-.5-.5-.5h-1.5c-.28 0-.5-.25-.5-.5s.39-1.58.47-1.84c.08-.26-.05-.54-.31-.63-.07-.02-.12-.04-.19-.03zm-4.47 3v4h1v-4h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Timer returns the "timer" Iconic.
func Timer() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>timer</title>
  <path d="M2 0v1h1v.03c-1.7.24-3 1.71-3 3.47 0 1.93 1.57 3.5 3.5 3.5s3.5-1.57 3.5-3.5c0-.45-.1-.87-.25-1.25l-.91.38c.11.29.16.57.16.88 0 1.39-1.11 2.5-2.5 2.5s-2.5-1.11-2.5-2.5 1.11-2.5 2.5-2.5c.3 0 .59.05.88.16l.34-.94c-.23-.08-.47-.12-.72-.16v-.06h1v-1h-3zm5 1.16s-3.65 2.81-3.84 3c-.19.2-.19.49 0 .69.19.2.49.2.69 0 .2-.2 3.16-3.69 3.16-3.69z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Transfer returns the "transfer" Iconic.
func Transfer() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>transfer</title>
  <path d="M6 0v1h-6v1h6v1l2-1.5-2-1.5zm-4 4l-2 1.5 2 1.5v-1h6v-1h-6v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Trash returns the "trash" Iconic.
func Trash() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>trash</title>
  <path d="M3 0c-.55 0-1 .45-1 1h-1c-.55 0-1 .45-1 1h7c0-.55-.45-1-1-1h-1c0-.55-.45-1-1-1h-1zm-2 3v4.81c0 .11.08.19.19.19h4.63c.11 0 .19-.08.19-.19v-4.81h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Underline returns the "underline" Iconic.
func Underline() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>underline</title>
  <path d="M1 0v4c0 1.1 1.12 2 2.5 2h.5c1.1 0 2-.9 2-2v-4h-1v4c0 .55-.45 1-1 1s-1-.45-1-1v-4h-2zm-1 7v1h7v-1h-7z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VerticalAlignBottom returns the "vertical-align-bottom" Iconic.
func VerticalAlignBottom() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>vertical-align-bottom</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v4.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-4.81c0-.06-.04-.09-.09-.09h-1.81zm6 0c-.05 0-.09.04-.09.09v4.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-4.81c0-.06-.04-.09-.09-.09h-1.81zm-3 2c-.06 0-.09.04-.09.09v2.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-2.81c0-.06-.04-.09-.09-.09h-1.81zm-3.09 4v1h8v-1h-8z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VerticalAlignCenter returns the "vertical-align-center" Iconic.
func VerticalAlignCenter() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>vertical-align-center</title>
  <path d="M.09 0c-.06 0-.09.04-.09.09v1.91h2v-1.91c0-.06-.04-.09-.09-.09h-1.81zm6 0c-.05 0-.09.04-.09.09v1.91h2v-1.91c0-.06-.04-.09-.09-.09h-1.81zm-3 1c-.06 0-.09.04-.09.09v.91h2v-.91c0-.05-.04-.09-.09-.09h-1.81zm-3.09 2v1h8v-1h-8zm0 2v1.91c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-1.91h-2zm3 0v.91c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-.91h-2zm3 0v1.91c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-1.91h-2z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VerticalAlignTop returns the "vertical-align-top" Iconic.
func VerticalAlignTop() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>vertical-align-top</title>
  <path d="M0 0v1h8v-1h-8zm.09 2c-.06 0-.09.04-.09.09v4.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-4.81c0-.06-.04-.09-.09-.09h-1.81zm3 0c-.06 0-.09.04-.09.09v2.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-2.81c0-.06-.04-.09-.09-.09h-1.81zm3 0c-.05 0-.09.04-.09.09v4.81c0 .05.04.09.09.09h1.81c.05 0 .09-.04.09-.09v-4.81c0-.06-.04-.09-.09-.09h-1.81z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Video returns the "video" Iconic.
func Video() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>video</title>
  <path d="M.5 0c-.28 0-.5.23-.5.5v4c0 .28.23.5.5.5h5c.28 0 .5-.22.5-.5v-1.5l1 1h1v-3h-1l-1 1v-1.5c0-.28-.22-.5-.5-.5h-5z" transform="translate(0 1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VolumeHigh returns the "volume-high" Iconic.
func VolumeHigh() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>volume-high</title>
  <path d="M3.34 0l-1.34 2h-2v4h2l1.34 2h.66v-8h-.66zm1.66 1v1c.17 0 .34.02.5.06.86.22 1.5 1 1.5 1.94s-.63 1.72-1.5 1.94c-.16.04-.33.06-.5.06v1c.25 0 .48-.04.72-.09h.03c1.3-.33 2.25-1.51 2.25-2.91 0-1.4-.95-2.58-2.25-2.91-.23-.06-.49-.09-.75-.09zm0 2v2c.09 0 .18-.01.25-.03.43-.11.75-.51.75-.97 0-.46-.31-.86-.75-.97-.08-.02-.17-.03-.25-.03z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VolumeLow returns the "volume-low" Iconic.
func VolumeLow() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>volume-low</title>
  <path d="M3.34 0l-1.34 2h-2v4h2l1.34 2h.66v-8h-.66zm1.66 3v2c.09 0 .18-.01.25-.03.43-.11.75-.51.75-.97 0-.46-.31-.86-.75-.97-.08-.02-.17-.03-.25-.03z" transform="translate(1)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// VolumeOff returns the "volume-off" Iconic.
func VolumeOff() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>volume-off</title>
  <path d="M3.34 0l-1.34 2h-2v4h2l1.34 2h.66v-8h-.66z" transform="translate(2)"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Warning returns the "warning" Iconic.
func Warning() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>warning</title>
  <path d="M3.09 0c-.06 0-.1.04-.13.09l-2.94 6.81c-.02.05-.03.13-.03.19v.81c0 .05.04.09.09.09h6.81c.05 0 .09-.04.09-.09v-.81c0-.05-.01-.14-.03-.19l-2.94-6.81c-.02-.05-.07-.09-.13-.09h-.81zm-.09 3h1v2h-1v-2zm0 3h1v1h-1v-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Wifi returns the "wifi" Iconic.
func Wifi() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>wifi</title>
  <path d="M3.75 0c-1.38 0-2.66.4-3.75 1.09l.53.81c.93-.59 2.03-.91 3.22-.91 1.2 0 2.32.31 3.25.91l.53-.81c-1.09-.7-2.4-1.09-3.78-1.09zm0 3c-.79 0-1.5.23-2.13.63l.53.84c.47-.3 1-.47 1.59-.47.59 0 1.16.17 1.63.47l.53-.84c-.62-.39-1.37-.63-2.16-.63zm0 3c-.55 0-1 .45-1 1s.45 1 1 1 1-.45 1-1-.45-1-1-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Wrench returns the "wrench" Iconic.
func Wrench() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>wrench</title>
  <path d="M5.5 0c-1.38 0-2.5 1.12-2.5 2.5 0 .32.08.62.19.91l-2.91 2.88c-.39.39-.39 1.05 0 1.44.2.2.46.28.72.28.26 0 .52-.09.72-.28l2.88-2.91c.28.11.58.19.91.19 1.38 0 2.5-1.12 2.5-2.5 0-.16 0-.32-.03-.47l-.97.97h-2v-2l.97-.97c-.15-.03-.31-.03-.47-.03zm-4.5 6.5c.28 0 .5.22.5.5s-.22.5-.5.5-.5-.22-.5-.5.22-.5.5-.5z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// X returns the "x" Iconic.
func X() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>x</title>
  <path d="M1.41 0l-1.41 1.41.72.72 1.78 1.81-1.78 1.78-.72.69 1.41 1.44.72-.72 1.81-1.81 1.78 1.81.69.72 1.44-1.44-.72-.69-1.81-1.78 1.81-1.81.72-.72-1.44-1.41-.69.72-1.78 1.78-1.81-1.78-.72-.72z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// Yen returns the "yen" Iconic.
func Yen() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>yen</title>
  <path d="M0 0l2.25 3h-2.25v1h3v1h-3v1h3v2h1v-2h3v-1h-3v-1h3v-1h-2.25l2.25-3h-1l-2.31 3h-.38l-2.31-3h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ZoomIn returns the "zoom-in" Iconic.
func ZoomIn() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>zoom-in</title>
  <path d="M3.5 0c-1.93 0-3.5 1.57-3.5 3.5s1.57 3.5 3.5 3.5c.61 0 1.19-.16 1.69-.44a1 1 0 0 0 .09.13l1 1.03a1.02 1.02 0 1 0 1.44-1.44l-1.03-1a1 1 0 0 0-.13-.09c.27-.5.44-1.08.44-1.69 0-1.93-1.57-3.5-3.5-3.5zm0 1c1.39 0 2.5 1.11 2.5 2.5 0 .59-.2 1.14-.53 1.56-.01.01-.02.02-.03.03a1 1 0 0 0-.06.03 1 1 0 0 0-.25.28c-.44.37-1.01.59-1.63.59-1.39 0-2.5-1.11-2.5-2.5s1.11-2.5 2.5-2.5zm-.5 1v1h-1v1h1v1h1v-1h1v-1h-1v-1h-1z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}

// ZoomOut returns the "zoom-out" Iconic.
func ZoomOut() *Iconic {
	return &Iconic{
		xml: `<svg xmlns="http://www.w3.org/2000/svg" width="%d" height="%d" viewBox="0 0 8 8" style="%s" id="%s" class="%s"><title>zoom-out</title>
  <path d="M3.5 0c-1.93 0-3.5 1.57-3.5 3.5s1.57 3.5 3.5 3.5c.61 0 1.19-.16 1.69-.44a1 1 0 0 0 .09.13l1 1.03a1.02 1.02 0 1 0 1.44-1.44l-1.03-1a1 1 0 0 0-.13-.09c.27-.5.44-1.08.44-1.69 0-1.93-1.57-3.5-3.5-3.5zm0 1c1.39 0 2.5 1.11 2.5 2.5 0 .59-.2 1.14-.53 1.56-.01.01-.02.02-.03.03a1 1 0 0 0-.06.03 1 1 0 0 0-.25.28c-.44.37-1.01.59-1.63.59-1.39 0-2.5-1.11-2.5-2.5s1.11-2.5 2.5-2.5zm-1.5 2v1h3v-1h-3z"></path>
</svg>`,
		width:  16,
		height: 16,
		style:  "display: inline-block; vertical-align: text-top; fill: currentColor;",
	}
}
