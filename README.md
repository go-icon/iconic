# iconic
https://useiconic.com/open

# Installation
`go get -u gitea.com/go-icon/iconic`

# Usage
```go
icon := iconic.AccountLogin()

// Get the raw XML
xml := icon.XML()

// Get something suitable to pass directly to an html/template
html := icon.HTML()
```

# Build
`go generate generate.go`

# New Versions
To update the version of iconic, simply change `iconicVersion` in `iconic_generate.go` and re-build.

# License
[MIT License](LICENSE)